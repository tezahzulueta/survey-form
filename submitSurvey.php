<?php 
	require 'PHPMailer/PHPMailerAutoload.php';
	//if "email" variable is filled out, send email
	 if (isset($_POST['survey']))  {
	 	$mail = new PHPMailer;

		$admin_email = 'bagong_bayani_awards@yahoo.com,nimfadg@yahoo.com,theresa@lozatech.com';
		$email = $_POST['email'];
		$subject = "BBFI Survey";

		$company = $_POST['company'];
		$name = $_POST['name'];
		
		$email_message = "<strong>COMPANY NAME:</strong>" . $company ."\n <br />";
		$email_message .= "<strong>NAME:</strong>" . $name ."\n <br /><br />";

		$email_message .= "<strong>1.1. OBJECTIVES OF THE STUDY</strong>"."\n <br />";

		$email_message .= "<strong>1. INTRODUCTION</strong>" . "\n <br />";
		$email_message .= "<strong>1.1. OBJECTIVES OF THE STUDY</strong>"."\n <br />";
		$email_message .= $_POST['objectives']."\n <br />";
		$email_message .= "<strong>1.2. METHODOLOGY</strong>"."\n <br />";
		$email_message .= $_POST['methodology']."\n <br />";
		$email_message .= "<strong>1.3. ORGANISATION OF THE REPORT</strong>"."\n <br />";
		$email_message .= $_POST['organisation']."\n\n <br /> <br />";

		$email_message .= "<strong>2. BACKGROUND ON THE BBFI AWARDS</strong>" . "\n <br />";
		$email_message .= "<strong>2.1. FOUNDING BY DOLE"."\n <br />";
		$email_message .= $_POST['founding']."\n <br />";
		$email_message .= "<strong>2.2. CONVERSION TO PRIVATE FOUNDATION</strong>"."\n <br />";
		$email_message .= $_POST['conversion']."\n <br />";
		$email_message .= "<strong>2.3. RATIONALE AND DESIGN OF THE AWARDS</strong>"."\n <br />";
		$email_message .= $_POST['rationale']."\n <br />";
		$email_message .= "<strong>2.4. CAPT. GREGORIO OCA AWARD</strong>"."\n <br />";
		$email_message .= $_POST['oca_award']."\n\n <br /> <br />";

		$email_message .= "<strong>3. IMPLEMENTATION OF THE BBFI AWARDS</strong>" . "\n <br />";
		$email_message .= "<strong>3.1. EVALUATION PERIOD</strong>"."\n <br />";
		$email_message .= $_POST['evaluation_period']."\n <br />";
		$email_message .= "<strong>3.2. AWARD CATEGORIES</strong>"."\n <br />";
		$email_message .= $_POST['award_categories']."\n <br />";
		$email_message .= "<strong>3.3. ELIGIBILITY, CRITERIA AND WEIGHT DISTRIBUTION</strong>"."\n <br />";
		$email_message .= $_POST['eligibility']."\n <br />";
		$email_message .= "<strong>3.4. INCENTIVES</strong>"."\n <br />";
		$email_message .= $_POST['incentives']."\n <br />";
		$email_message .= "<strong>3.5. THE EVALUATION PROCESS</strong>"."\n <br />";
		$email_message .= $_POST['evaluation_process']."\n <br />";
		$email_message .= "<strong>3.6. THE AWARDEES: 1984-2014</strong>"."\n <br />";
		$email_message .= $_POST['awardees']."\n\n <br /> <br />";

		$email_message .= "<strong>4. VIEWS AND PERSPECTIVES OF STAKEHOLDERS</strong>" . "\n <br />";
		$email_message .= "<strong>4.1. KNOWLEDGE AND AWARENESS OF THE AWARDS</strong>"."\n <br />";
		$email_message .= $_POST['awareness']."\n <br />";
		$email_message .= "<strong>4.2. CRITIQUE OF THE CRITERIA AND THE WEIGHT DISTRIBUTION</strong>"."\n <br />";
		$email_message .= $_POST['criteria']."\n <br />";
		$email_message .= "<strong>4.3. VIEWS OF THE SPONSORS AND DONORS</strong>"."\n <br />";
		$email_message .= $_POST['views']."\n <br />";
		$email_message .= "<strong>4.4. THE IMPACT ON THE INDUSTRY RECRUITMENT MANNING</strong>"."\n <br />";
		$email_message .= $_POST['recruitment']."\n <br />";
		$email_message .= "<strong>4.5. THE IMPACT ON FOREIGN EMPLOYERS</strong>"."\n <br />";
		$email_message .= $_POST['foreign_employers']."\n <br />";
		$email_message .= "<strong>4.6. THE IMPACT ON THE PHILIPPINES AND CORRESPONDING RECEIVING COUNTRIES</strong>"."\n <br />";
		$email_message .= $_POST['receiving_countries']."\n\n <br /> <br />";

		$email_message .= "<strong>5. HIGHLIGHTS FROM THE VALIDATION WORKSHOP</strong>"."\n <br />";
		$email_message .= $_POST['validation_workshop']."\n <br />";

		$email_message .= "<strong>6. CONCLUSION AND RECOMMMENDATION</strong>"."\n <br />";
		$email_message .= $_POST['conclusion']."\n\n <br /> <br />";

		$email_message .= "<strong>7. REFERENCES</strong>"."\n <br />";
		$email_message .= $_POST['references']."\n\n <br /> <br />";

		$email_message .= "<strong>ADDITIONAL COMMENTS/SUGGESTIONS, IF ANY.</strong>"."\n <br />";
		$email_message .= $_POST['suggestions']."\n\n <br /> <br />";


		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = 'bbfi.com.ph';  						// Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = 'admin@bbfi.com.ph';                 // SMTP username
		$mail->Password = '4dTKi)(!TOc[';                           // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to

		$mail->From = $email;
		$mail->FromName = $name;
		$mail->addAddress('bagong_bayani_awards@yahoo.com');
		$mail->addCC('nimfadg@yahoo.com');
		$mail->addBCC('theresa@lozatech.com');

		$mail->isHTML(true);                                  // Set email format to HTML

		$mail->Subject = 'BBFI Survey';
		$mail->Body    = $email_message;
		// $mail->AltBody = $email_message;

		if(!$mail->send()) {
		    echo 'Message could not be sent.';
		    echo 'Mailer Error: ' . $mail->ErrorInfo;
		} else {
		    echo 'Message has been sent';
		}
		// mail($admin_email, $subject, $email_message, "From:" . $email);
		  
		// //Email response
		// echo "Thank youfdgdf!";
	}

?>