<?php 
	require 'PHPMailer/PHPMailerAutoload.php';
	//if "email" variable is filled out, send email
	 if (isset($_POST['survey']))  {
	 	$mail = new PHPMailer;

		$admin_email = 'bagong_bayani_awards@yahoo.com,nimfadg@yahoo.com,theresa@lozatech.com';
		$email = $_POST['email'];
		$subject = "BBFI Survey";

		$company = $_POST['company'];
		$name = $_POST['name'];
		
		$email_message = "COMPANY NAME:" . $company ."\n";
		$email_message .= "NAME:" . $name ."\n";

		$email_message .= "1.1. OBJECTIVES OF THE STUDY"."\n";

		$email_message .= "1. INTRODUCTION" . "\n";
		$email_message .= "1.1. OBJECTIVES OF THE STUDY"."\n";
		$email_message .= $_POST['objectives']."\n";
		$email_message .= "1.2. METHODOLOGY"."\n";
		$email_message .= $_POST['methodology']."\n";
		$email_message .= "1.3. ORGANISATION OF THE REPORT"."\n";
		$email_message .= $_POST['organisation']."\n\n";

		$email_message .= "2. BACKGROUND ON THE BBFI AWARDS" . "\n";
		$email_message .= "2.1. FOUNDING BY DOLE"."\n";
		$email_message .= $_POST['founding']."\n";
		$email_message .= "2.2. CONVERSION TO PRIVATE FOUNDATION"."\n";
		$email_message .= $_POST['conversion']."\n";
		$email_message .= "2.3. RATIONALE AND DESIGN OF THE AWARDS"."\n";
		$email_message .= $_POST['rationale']."\n";
		$email_message .= "2.4. CAPT. GREGORIO OCA AWARD"."\n";
		$email_message .= $_POST['oca_award']."\n\n";

		$email_message .= "3. IMPLEMENTATION OF THE BBFI AWARDS" . "\n";
		$email_message .= "3.1. EVALUATION PERIOD"."\n";
		$email_message .= $_POST['evaluation_period']."\n";
		$email_message .= "3.2. AWARD CATEGORIES"."\n";
		$email_message .= $_POST['award_categories']."\n";
		$email_message .= "3.3. ELIGIBILITY, CRITERIA AND WEIGHT DISTRIBUTION"."\n";
		$email_message .= $_POST['eligibility']."\n";
		$email_message .= "3.4. INCENTIVES"."\n";
		$email_message .= $_POST['incentives']."\n";
		$email_message .= "3.5. THE EVALUATION PROCESS"."\n";
		$email_message .= $_POST['evaluation_process']."\n";
		$email_message .= "3.6. THE AWARDEES: 1984-2014"."\n";
		$email_message .= $_POST['awardees']."\n\n";

		$email_message .= "4. VIEWS AND PERSPECTIVES OF STAKEHOLDERS" . "\n";
		$email_message .= "4.1. KNOWLEDGE AND AWARENESS OF THE AWARDS"."\n";
		$email_message .= $_POST['awareness']."\n";
		$email_message .= "4.2. CRITIQUE OF THE CRITERIA AND THE WEIGHT DISTRIBUTION"."\n";
		$email_message .= $_POST['criteria']."\n";
		$email_message .= "4.3. VIEWS OF THE SPONSORS AND DONORS"."\n";
		$email_message .= $_POST['views']."\n";
		$email_message .= "4.4. THE IMPACT ON THE INDUSTRY RECRUITMENT MANNING"."\n";
		$email_message .= $_POST['recruitment']."\n";
		$email_message .= "4.5. THE IMPACT ON FOREIGN EMPLOYERS"."\n";
		$email_message .= $_POST['foreign_employers']."\n";
		$email_message .= "4.6. THE IMPACT ON THE PHILIPPINES AND CORRESPONDING RECEIVING COUNTRIES"."\n";
		$email_message .= $_POST['receiving_countries']."\n\n";

		$email_message .= "5. HIGHLIGHTS FROM THE VALIDATION WORKSHOP"."\n";
		$email_message .= $_POST['validation_workshop']."\n";

		$email_message .= "6. CONCLUSION AND RECOMMMENDATION"."\n";
		$email_message .= $_POST['conclusion']."\n\n";

		$email_message .= "7. REFERENCES"."\n";
		$email_message .= $_POST['references']."\n\n";

		$email_message .= "ADDITIONAL COMMENTS/SUGGESTIONS, IF ANY."."\n";
		$email_message .= $_POST['suggestions']."\n\n";		


		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = 'bbfi.com.ph';  						// Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = 'admin@bbfi.com.ph';                 // SMTP username
		$mail->Password = '4dTKi)(!TOc[';                           // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to

		$mail->From = $email;
		$mail->FromName = $name;
		$mail->addAddress('bagong_bayani_awards@yahoo.com');   
		$mail->addCC('nimfadg@yahoo.com');
		$mail->addBCC('theresa@lozatech.com');

		$mail->isHTML(true);                                  // Set email format to HTML

		$mail->Subject = 'BBFI Survey';
		$mail->Body    = "COMPANY NAME:" . $company ."\n" . "NAME:" . $name ."\n";
		// $mail->AltBody = $email_message;

		if(!$mail->send()) {
		    echo 'Message could not be sent.';
		    echo 'Mailer Error: ' . $mail->ErrorInfo;
		} else {
		    echo 'Message has been sent';
		}
		// mail($admin_email, $subject, $email_message, "From:" . $email);
		  
		// //Email response
		// echo "Thank youfdgdf!";
	}

?>